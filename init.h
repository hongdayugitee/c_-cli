//
// Created by hongs on 2021/8/21.
//

#ifndef NEWCGI_INIT_H
#define NEWCGI_INIT_H
typedef void (*driver_init)(void);
extern driver_init driver_init_start;
extern driver_init driver_init_end;

typedef void (*server_init)(void);
extern server_init server_init_start;
extern server_init server_init_end;

#define SERVER_CALL_END server_init_end
#define DRIVER_CALL_END driver_init_end

#define __INIT_DRIVER __attribute__((unused, section(".driver_init")))
#define __INIT_SERVER __attribute__((unused,section(".server_init")))

#define DRIVER_INIT(driver_name) \
driver_init _fn_##driver_name __INIT_DRIVER = driver_name

#define SERVER_INIT(server_name) \
server_init _fn_##server_name __INIT_SERVER = server_name

#define __INIT_FUNC(name) \
do{ \
(*name##_CALL_INIT)(); \
++name##_CALL_INIT; \
}while(name##_CALL_INIT < &name##_CALL_END)

#endif //NEWCGI_INIT_H
