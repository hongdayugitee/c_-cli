//
// Created by hongs on 2021/8/20.
//

#ifndef NEWCGI_SQLDB_H
#define NEWCGI_SQLDB_H

#include<stdio.h>
#include<sqlite3.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>

/* sqlite的查询和插入语句 */
/* 可以随机插入语句到相应的数据表中 */
/* 数据库底层 读写接口初始化 */
/* 目前使用的数值在初始化之前在做初始化 */

#define CREATE_SQL "CREATE TABLE"
#define INSERT_SQL "INSERT INTO"

#define FATAL do { fprintf(stderr, "Error at line %d, file %s (%d) [%s]\n", \
__LINE__, __FILE__, errno, strerror(errno)); exit(1); } while(0)

/*对sql的使用采用 如果脚本可以导入，就使用脚本导入 数据表完成数据库的初始化,对sqlit3的操作*/

static inline char* create_db(const char *sql,const char *table, sqlite3 *db)
{
    char cmd[256] = {0};
    int ret = 0;
    char *errMsg;
    sprintf(cmd,"%s  %s%s",CREATE_SQL,table,sql);
    //printf("cmd:%s\n",cmd);
    ret = sqlite3_exec(db,cmd,NULL,NULL,&errMsg); //应该是一个关于错误的全局错误变量
    if (ret){
        printf("sqlite3 errMsg: %s\n",errMsg);
        return errMsg;
    };
    return "SQLITE_CREATE_OK";
}

static inline void insert_db(const char *sql,const char *table,sqlite3 *db)
{
    int rc;
    char *cmd = (char*)calloc(strlen(sql)+256,sizeof(char));
    if(cmd == NULL){
        printf("calloc cmd sql error");
        return;
    }
    char *errMsg;
    sprintf(cmd,"%s %s %s;",INSERT_SQL,table,sql); //需要插入的数据表的值,需要插入的数据的值
    //printf("insert cmd:%s\n",cmd);
    rc = sqlite3_exec(db,cmd,NULL,NULL,&errMsg);
    free(cmd);
    cmd = NULL;
    if(rc){
        printf("sqlite3 insert error；%s\n",errMsg);
    }
}


static inline void update_db(unsigned int id,char *data,const char* table,sqlite3 *db) //设置不同的ID值进行赋值
{
    char temp[256] = {0};
    int ret;
    sprintf(temp,"UPDATE %s set value = '%s' where ID= '%#x'",table,data,id); //id值更新相应的数据
    ret = sqlite3_exec(db,temp,NULL,NULL,NULL);
    if(ret) FATAL;
}

static inline void update_com(char *sql,sqlite3*db){
    char *errMsg;
    int ret = sqlite3_exec(db,sql,NULL,NULL,&errMsg);
    //printf("sql %s",sql);
    if(ret){
        printf("sqlite3 update eror:%s\n",errMsg);
    }
}

static inline void delete_db(unsigned int id,const char *table,sqlite3 *db) //删除某个ID的数据值
{
    char temp[256] = {0};
    sprintf(temp,"DELETE FROM %s where ID= \'%#x\'\n",table,id); //删除某一个ID值关联的记录,附带的整个数据记录
    int ret;
    ret = sqlite3_exec(db,temp,NULL,NULL,NULL);
    if(ret) FATAL;
}

static inline void delete_table_db(const char* table,sqlite3 *db) //删除某个数据表进行操作
{
    char temp[256] = {0};
    sprintf(temp,"DELETE * FROM %s",table);
    int ret;
    ret = sqlite3_exec(db,temp,NULL,NULL,NULL);
    if(ret) FATAL;
}

#endif